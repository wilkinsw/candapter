/*
 * candapter.c
 *
 * Driver for the Ewert Energy Systems CANdapter
 *
 * Copyright (c) 2017 Wilkins White <ww@novadynamics.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the version 2 of the GNU General Public License
 * as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/can.h>
#include <linux/can/dev.h>
#include <linux/can/skb.h>
#include <linux/slab.h>
#include <linux/usb.h>


/***************************************************
        Candapter Defines
****************************************************/

#define VENDOR_ID       0x0403
#define PRODUCT_ID      0x6001

#define MAX_RX_URBS     20
#define MAX_TX_URBS     10
#define RX_BUFFER_SIZE  64

enum candapter_baudrate {
    BAUD_10K,
    BAUD_20K,
    BAUD_50K,
    BAUD_100K,
    BAUD_125K,
    BAUD_250K,
    BAUD_500K,
    BAUD_800K,
    BAUD_1M
};


/***************************************************
        Candapter Structures
****************************************************/

struct candapter_context {
    struct candapter_priv       *priv;
    uint32_t                    echo_index;
    uint8_t                     dlc;
};

struct candapter_rx_message {
    uint16_t                    id;
    uint8_t                     dlc;
    uint8_t                     data[8];
};


struct candapter_priv {
    struct can_priv             can;
    struct net_device           *net;
    struct usb_device           *usb;
    struct device               *dev;
    struct sk_buff              *echo_socket_buffer[MAX_TX_URBS];
    atomic_t                    active_tx_urbs;
    struct usb_anchor           tx_submitted;
    struct candapter_context    tx_contexts[MAX_TX_URBS];
    struct usb_anchor           rx_submitted;
    struct can_berr_counter     bec;
    enum candapter_baudrate     baud;
};


/***************************************************
        Candapter Prototypes
****************************************************/

static inline uint16_t candapter_atoh(char *buffer, uint8_t len);
static int candapter_send(struct candapter_priv *priv, uint8_t *msg, int size);
static int candapter_set_baud(struct candapter_priv *priv, enum candapter_baudrate baud);
static int candapter_bus_open(struct candapter_priv *priv);
static int candapter_bus_close(struct candapter_priv *priv);
static void candapter_handle_rx_msg(struct candapter_priv *priv, struct candapter_rx_message *msg);
static void candapter_usb_read(struct urb *urb);
static void candapter_usb_write(struct urb *urb);
static int candapter_usb_start(struct candapter_priv *priv);
static int candapter_ndo_open(struct net_device *net);
static int candapter_ndo_stop(struct net_device *net);
static netdev_tx_t candapter_ndo_start(struct sk_buff *skb, struct net_device *net);
static int candapter_set_mode(struct net_device *net, enum can_mode mode);
static int candapter_get_berr_counter(const struct net_device *net, struct can_berr_counter *bec);
static int candapter_probe(struct usb_interface *interface, const struct usb_device_id *id);
static void candapter_disconnect(struct usb_interface *interface);


/***************************************************
        Candapter Helper Functions
****************************************************/

/**
 * candapter_atoh - converts an ascii-encoded hex string into its value.
 * @buffer: pointer to string location.
 * @len:    length of string.
 */
static inline uint16_t candapter_atoh(char *buffer, uint8_t len)
{
    uint16_t power;
    uint16_t val = 0;
    uint8_t i, j;

    for(i = 0; i < len; i++)
    {
        power = 1;
        for(j=0; j < (len-i-1); j++) power *= 16;

        if(buffer[i] >= '0' && buffer[i] <= '9')
        {
            val += (buffer[i] - '0') * power;
        }
        else if(buffer[i] >= 'A' && buffer[i] <= 'F')
        {
            val += (buffer[i] - 'A' + 10) * power;
        }
        else if(buffer[i] >= 'a' && buffer[i] <= 'f')
        {
            val += (buffer[i] - 'a' + 10) * power;
        }
    }

    return val;
}

/**
 * candapter_send - sends a one time message to the CANdapter.
 * @priv:   pointer to private structure.
 * @msg:    pointer to message buffer.
 * @size:   number of characters to send.
 */
static int candapter_send(struct candapter_priv *priv, uint8_t *msg, int size)
{
    int ret, actual_length;

    ret = usb_bulk_msg(priv->usb, usb_sndbulkpipe(priv->usb, 2), msg, size, &actual_length, 1000);
    if(ret < 0) return ret;
    else return (size - actual_length);
}

/**
 * candapter_handle_rx_msg - converts a candapter message to a can_frame and submits it.
 * @priv:   pointer to private structure.
 * @msg:    pointer to a candapter message.
 */
static void candapter_handle_rx_msg(struct candapter_priv *priv, struct candapter_rx_message *msg)
{
    struct can_frame *cf;
    struct sk_buff *skb;
    struct net_device_stats *stats = &priv->net->stats;

    skb = alloc_can_skb(priv->net, &cf);
    if(!skb) return;

    cf->can_id = msg->id;
    cf->can_dlc = get_can_dlc(msg->dlc & 0xF);
    memcpy(cf->data, msg->data, cf->can_dlc);

    netif_rx(skb);
    stats->rx_packets++;
    stats->rx_bytes += cf->can_dlc;
}


/***************************************************
        Candapter Commands
****************************************************/

/**
 * candapter_set_baud - sets the candapter baudrate
 * @priv:   pointer to private structure.
 * @baud:   baud rate to set
 */
static int candapter_set_baud(struct candapter_priv *priv, enum candapter_baudrate baud)
{
    char buffer[5];

    dev_dbg(priv->dev, "%s\n", __func__);

    priv->baud = baud;
    sprintf(buffer, "\rS%1d\r", baud);
    return candapter_send(priv, buffer, 4);
}

/**
 * candapter_bus_open - opens the candapter bus
 * @priv:   pointer to private structure.
 */
static int candapter_bus_open(struct candapter_priv *priv)
{
    char buffer[4];

    dev_dbg(priv->dev, "%s\n", __func__);

    sprintf(buffer, "\rO\r");
    return candapter_send(priv, buffer, 3);
}

/**
 * candapter_bus_close - closes the candapter bus
 * @priv:   pointer to private structure.
 */
static int candapter_bus_close(struct candapter_priv *priv)
{
    char buffer[4];

    dev_dbg(priv->dev, "%s\n", __func__);

    sprintf(buffer, "\rC\r");
    return candapter_send(priv, buffer, 3);
}


/***************************************************
        Candapter USB Operations
****************************************************/

/**
 * candapter_usb_read - callback for when a rx urb is ready.
 * @urb:    urb to unpack
 */
static void candapter_usb_read(struct urb *urb)
{
    struct candapter_rx_message msg;
    struct candapter_priv *priv = urb->context;
    struct net_device *net = priv->net;
    char *start;
    int ret = 0;
    int i;

    if(!netif_device_present(net)) return;

    switch(urb->status)
    {
        case 0:

            if(urb->actual_length > 6)
            {
                start = (char *)memchr(urb->transfer_buffer, 't', urb->actual_length);
                if(start != NULL)
                {
                    msg.id = candapter_atoh(start+1, 3);
                    //dev_dbg(priv->dev, "msg.id=%03X\n", msg.id);
                    msg.dlc = (uint8_t)(*(start+4)) - '0';
                    //dev_dbg(priv->dev, "msg.dlc = %X", msg.dlc);
                    for(i=0; i < msg.dlc; i++)
                    {
                        msg.data[i] = candapter_atoh(start+5+2*i, 2);
                        //dev_dbg(priv->dev, "msg.data[%d] = %X", i, msg.data[i]);
                    }
                    candapter_handle_rx_msg(priv, &msg);
                }
            }

            break;
        case -ENOENT:
        case -ESHUTDOWN:
            return;

        default:
            netdev_info(net, "Rx URB aborted (%d)\n", urb->status);
            goto resubmit_urb;
    }

resubmit_urb:
    usb_fill_bulk_urb(urb, priv->usb, usb_rcvbulkpipe(priv->usb, 1), urb->transfer_buffer, RX_BUFFER_SIZE, candapter_usb_read, priv);
    ret = usb_submit_urb(urb, GFP_ATOMIC);
    if(ret == -ENODEV) netif_device_detach(net);
    else if(ret) netdev_err(net, "failed to resubmit read bulk urb: %d\n", ret);
}

/**
 * candapter_usb_write - callback for when a tx urb is submitted.
 * @urb:    urb to send
 */
static void candapter_usb_write(struct urb *urb)
{
    struct candapter_context *context = urb->context;
    struct candapter_priv *priv;
    struct net_device *net;

    BUG_ON(!context);

    priv = context->priv;
    net = priv->net;

    dev_dbg(priv->dev, "%s\n", __func__);

    usb_free_coherent(urb->dev, urb->transfer_buffer_length, urb->transfer_buffer, urb->transfer_dma);
    atomic_dec(&priv->active_tx_urbs);

    if(!netif_device_present(net)) return;

    if(urb->status) netdev_info(net, "Tx URB aborted (%d)\n", urb->status);

    net->stats.tx_packets++;
    net->stats.tx_bytes += context->dlc;

    can_get_echo_skb(net, context->echo_index);
    context->echo_index = MAX_TX_URBS;
    netif_wake_queue(net);
}

/**
 * candapter_usb_start - initializes the usb and sets up receive urbs.
 * @priv:   pointer to private structure.
 */
static int candapter_usb_start(struct candapter_priv *priv)
{
    struct net_device *net = priv->net;
    struct urb *urb = NULL;
    int ret = 0;
    uint8_t *buffer;
    uint8_t i;

    dev_dbg(priv->dev, "%s\n", __func__);

    for(i=0; i < MAX_RX_URBS; i++)
    {
        urb = usb_alloc_urb(0, GFP_KERNEL);
        if(!urb)
        {
            netdev_err(net, "No memory left for URBs\n");
            ret = -ENOMEM;
            break;
        }

        buffer = usb_alloc_coherent(priv->usb, RX_BUFFER_SIZE, GFP_KERNEL, &urb->transfer_dma);
        if(!buffer)
        {
            netdev_err(net, "No memory left for URB buffer\n");
            usb_free_urb(urb);
            ret = -ENOMEM;
            break;
        }

        usb_fill_bulk_urb(urb, priv->usb, usb_rcvbulkpipe(priv->usb, 1), buffer, RX_BUFFER_SIZE, candapter_usb_read, priv);
        urb->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;
        usb_anchor_urb(urb, &priv->rx_submitted);

        ret = usb_submit_urb(urb, GFP_KERNEL);
        if(ret)
        {
            netdev_err(net, "Failed to submit URB\n");
            usb_unanchor_urb(urb);
            usb_free_coherent(priv->usb, RX_BUFFER_SIZE, buffer, urb->transfer_dma);
            usb_free_urb(urb);
            break;
        }
        usb_free_urb(urb);
    }

    return ret;
}

/***************************************************
        Candapter Network Device Operations
****************************************************/

/**
 * candapter_ndo_open - called when the network device is opened.
 * @net:    pointer to the net_device.
 */
static int candapter_ndo_open(struct net_device *net)
{
    struct candapter_priv *priv = netdev_priv(net);
    int ret = 0;

    dev_dbg(priv->dev, "%s\n", __func__);

    ret = open_candev(net);
    if(ret) return ret;

    ret = candapter_usb_start(priv);
    if(ret)
    {
        if(ret == -ENODEV) netif_device_detach(priv->net);
        netdev_err(net, "error starting usb: %d\n", ret);
        close_candev(net);
        return ret;
    }

    ret = candapter_set_mode(net, CAN_MODE_START);
    if(ret) netdev_warn(net, "couldn't open bus: %d\n", ret);

    netif_start_queue(net);

    return ret;
}

/**
 * candapter_ndo_stop - called when the network device is closed.
 * @net:    pointer to the net_device.
 */
static int candapter_ndo_stop(struct net_device *net)
{
    struct candapter_priv *priv = netdev_priv(net);
    int ret = 0;
    int i = 0;

    dev_dbg(priv->dev, "%s\n", __func__);

    ret = candapter_set_mode(net, CAN_MODE_STOP);
    netif_stop_queue(net);

    usb_kill_anchored_urbs(&priv->rx_submitted);
    usb_kill_anchored_urbs(&priv->tx_submitted);
    atomic_set(&priv->active_tx_urbs, 0);

    for(i=0; i < MAX_TX_URBS; i++)
    {
        priv->tx_contexts[i].echo_index = MAX_TX_URBS;
    }

    close_candev(net);
    return ret;
}

/**
 * candapter_ndo_start - called when a message is sent through the network device.
 * @skb:    message data to send.
 * @net:    pointer to the net_device.
 */
static netdev_tx_t candapter_ndo_start(struct sk_buff *skb, struct net_device *net)
{
    struct candapter_priv *priv = netdev_priv(net);
    struct net_device_stats *stats = &net->stats;
    struct can_frame *cf = (struct can_frame *) skb->data;
    struct candapter_context *context = NULL;
    struct urb *urb;
    char *buffer;
    int i;
    int ret;
    int size;

    dev_dbg(priv->dev, "%s\n", __func__);

    if(can_dropped_invalid_skb(net, skb)) return NETDEV_TX_OK;

    urb = usb_alloc_urb(0, GFP_ATOMIC);
    if(!urb)
    {
        netdev_err(net, "No memory left for URBs\n");
        dev_kfree_skb(skb);
        stats->tx_dropped++;
        return NETDEV_TX_OK;
    }

    size = (6 + 2*cf->can_dlc);
    buffer = usb_alloc_coherent(priv->usb, size, GFP_ATOMIC, &urb->transfer_dma);
    if(!buffer)
    {
        netdev_err(net, "No memory left for URB buffer\n");
        usb_free_urb(urb);
        dev_kfree_skb(skb);
        stats->tx_dropped++;
        return NETDEV_TX_OK;
    }

    memcpy(buffer + sprintf(buffer, "T%3X%X", (uint16_t)cf->can_id, cf->can_dlc), cf->data, cf->can_dlc);
    buffer[size-1] = '\r';

    for(i=0; i < MAX_TX_URBS; i++)
    {
        if(priv->tx_contexts[i].echo_index == MAX_TX_URBS)
        {
            context = &priv->tx_contexts[i];
            break;
        }
    }

    if(!context)
    {
        usb_free_coherent(priv->usb, size, buffer, urb->transfer_dma);
        usb_free_urb(urb);

        netdev_warn(net, "no free context\n");

        return NETDEV_TX_BUSY;
    }

    context->priv = priv;
    context->echo_index = i;
    context->dlc = cf->can_dlc;

    usb_fill_bulk_urb(urb, priv->usb, usb_sndbulkpipe(priv->usb, 2), buffer, size, candapter_usb_write, context);
    urb->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;
    usb_anchor_urb(urb, &priv->tx_submitted);

    can_put_echo_skb(skb, net, context->echo_index);

    atomic_inc(&priv->active_tx_urbs);

    ret = usb_submit_urb(urb, GFP_ATOMIC);
    if(unlikely(ret))
    {
        can_free_echo_skb(net, context->echo_index);
        usb_unanchor_urb(urb);
        usb_free_coherent(priv->usb, size, buffer, urb->transfer_dma);

        atomic_dec(&priv->active_tx_urbs);
        if(ret == -ENODEV) netif_device_detach(net);
        else netdev_warn(net, "failed tx urb: %d\n", ret);
    }
    else if(atomic_read(&priv->active_tx_urbs) >= MAX_TX_URBS) netif_stop_queue(net);

    usb_free_urb(urb);

    return NETDEV_TX_OK;
}

static const struct net_device_ops candapter_netdev_ops = {
    .ndo_open           = candapter_ndo_open,
    .ndo_stop           = candapter_ndo_stop,
    .ndo_start_xmit     = candapter_ndo_start,
};

/***************************************************
        Candapter CAN Bus Operations
****************************************************/

static const struct can_bittiming_const candapter_bittiming_const = {
    .name       = "candapter",
    .tseg1_min  = 1,
    .tseg1_max  = 16,
    .tseg2_min  = 2,
    .tseg2_max  = 8,
    .sjw_max    = 4,
    .brp_min    = 1,
    .brp_max    = 1024,
    .brp_inc    = 1,
};

/**
 * candapter_set_mode - used to open or close the candapter bus.
 * @net:    pointer to the net_device.
 * @mode:   can_mode to set
 */
static int candapter_set_mode(struct net_device *net, enum can_mode mode)
{
    int ret = 0;
    struct candapter_priv *priv = netdev_priv(net);

    dev_dbg(priv->dev, "%s\n", __func__);

    switch(mode)
    {
        case CAN_MODE_START:
            ret = candapter_bus_open(priv);
            if(!ret) priv->can.state = CAN_STATE_ERROR_ACTIVE;
            break;

        case CAN_MODE_STOP:
            ret = candapter_bus_close(priv);
            if(!ret) priv->can.state = CAN_STATE_STOPPED;
            break;

        default:
            ret = -EOPNOTSUPP;
    }

    return ret;
}

/**
 * candapter_get_berr_counter - gets the can bus errors.
 * @net:    pointer to the net_device.
 * @bec:    pointer to can_berr_counter
 */
static int candapter_get_berr_counter(const struct net_device *net, struct can_berr_counter *bec)
{
    struct candapter_priv *priv = netdev_priv(net);
    bec->txerr = priv->bec.txerr;
    bec->rxerr = priv->bec.rxerr;
    return 0;
}

/***************************************************
        Candapter Module Operations
****************************************************/

/**
 * candapter_probe - called when the device is first registered witht he system.
 * @interface:  pointer to the usb_interface.
 * @id:         pointer to the usb_device_id.
 */
static int candapter_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
    int ret, i;
    struct net_device *net;
    struct candapter_priv * priv;
    struct usb_device *usb = interface_to_usbdev(interface);

    dev_dbg(&interface->dev, "%s\n", __func__);

    net = alloc_candev(sizeof(struct candapter_priv), 1);
    if(!net) return -ENOMEM;

    net->netdev_ops = &candapter_netdev_ops;
    net->flags |= IFF_ECHO;

    priv = netdev_priv(net);
    priv->can.state = CAN_STATE_STOPPED;
    priv->can.bittiming_const = &candapter_bittiming_const;
    priv->can.do_set_mode = candapter_set_mode;
    priv->can.do_get_berr_counter = candapter_get_berr_counter;
    priv->can.clock.freq = 32000000;
    priv->can.ctrlmode_supported = CAN_CTRLMODE_LOOPBACK | CAN_CTRLMODE_LISTENONLY;
    priv->net = net;
    priv->usb = usb;
    priv->dev = &interface->dev;

    init_usb_anchor(&priv->rx_submitted);
    init_usb_anchor(&priv->tx_submitted);
    atomic_set(&priv->active_tx_urbs, 0);

    for(i=0; i < MAX_TX_URBS; i++)
    {
        priv->tx_contexts[i].echo_index = MAX_TX_URBS;
    }

    usb_set_intfdata(interface, priv);
    SET_NETDEV_DEV(net, &interface->dev);
    ret = register_candev(net);
    if(ret)
    {
        dev_err(&interface->dev, "probe failed.\n");
        free_candev(net);
    }

    ret = candapter_set_baud(priv, BAUD_500K);
    if(ret) dev_warn(&interface->dev, "failed to set baudrate.\n");
    else dev_info(&interface->dev, "probe successful.\n");

    return ret;
}

/**
 * candapter_disconnect - called when the device unplugged from the system.
 * @interface:  pointer to the usb_interface.
 */
static void candapter_disconnect(struct usb_interface *interface)
{
    struct candapter_priv *priv = usb_get_intfdata(interface);
    struct net_device *net = priv->net;

    dev_dbg(&interface->dev, "%s\n", __func__);

    usb_set_intfdata(interface, NULL);
    if(priv)
    {
        unregister_candev(net);
        free_candev(net);
    }
}

static struct usb_device_id candapter_id_table [] = {
    {USB_DEVICE(VENDOR_ID, PRODUCT_ID)},
    {},
};

static struct usb_driver candapter_driver = {
    .name           = "candapter",
    .probe          = candapter_probe,
    .disconnect     = candapter_disconnect,
    .id_table       = candapter_id_table,
};


/***************************************************
        Module Setup
****************************************************/

module_usb_driver(candapter_driver);
MODULE_DEVICE_TABLE(usb, candapter_id_table);
MODULE_DESCRIPTION("Driver for the Ewert Energy Systems CANdapter");
MODULE_AUTHOR("Wilkins White <ww@novadynamics.com>");
MODULE_LICENSE("GPL v2");

