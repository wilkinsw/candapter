obj-m += candapter.o
ccflags-y=-I/usr/include

UDEV_RULE:= 99-candapter.rules

KDIR:= /lib/modules/$(shell uname -r)
PWD:= $(shell pwd)

default:
	$(MAKE) -C $(KDIR)/build M=$(PWD) modules

install: default
	sudo $(MAKE) -C $(KDIR)/build M=$(PWD) modules_install
	sudo depmod -a
	sudo cp $(UDEV_RULE) /etc/udev/rules.d/

uninstall:
	sudo rm /etc/udev/rules.d/$(UDEV_RULE)
	sudo udevadm control -R
	sudo rm $(KDIR)/extra/candapter.ko
	sudo depmod -a

clean:
	$(RM) *.o *.mod.c modules.order Module.symvers
