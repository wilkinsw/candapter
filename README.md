Obsolete.  Use the slcan daemon included in can-utils (http://elinux.org/Bringing_CAN_interface_up)
===================================================================================================

Candapter
=========
Linux driver for the Ewert Energy Systems CANBUS to USB converter.  Registers the candapter as a can network interface.  Only supports basic can messages, no support yet for RTR, error frames, or extended frames.

Installation
------------
Install build dependencies.
```bash
apt-get install build-essential
```

Build and install module from source.  This will compile the code against your running kernel and register it as a module.  It will also copy the default udev rule into /etc/udev/rules.d/.
```bash
git clone https://github.com/DaxBot/candapter.git
cd candapter
make
make install
```


Setup
-----
Unplug and replug the device to trigger the udev rule.  The rule should unbind the device from the ftdi_sio driver and bind it to the candapter driver.  Confirm this with dmesg.

```bash
ubuntu@tegra-ubuntu:~$ dmesg | grep ftdi_sio
[    3.235469] usbcore: registered new interface driver ftdi_sio
[    4.013324] ftdi_sio 1-3:1.0: FTDI USB Serial Device converter detected
[    9.543416] ftdi_sio ttyUSB0: FTDI USB Serial Device converter now disconnected from ttyUSB0
[    9.543442] ftdi_sio 1-3:1.0: device disconnected
```

```bash
ubuntu@tegra-ubuntu:~$ dmesg | grep candapter
[   10.570442] candapter 1-3:1.0: candapter_probe
[   10.570657] candapter 1-3:1.0: candapter_set_baud
[   10.571678] candapter 1-3:1.0: probe successful.
[   10.571737] usbcore: registered new interface driver candapter
[  818.093741] candapter 1-3:1.0: candapter_disconnect
```

Once the driver is loaded you can bring up the can0 interface.
```bash
sudo ip link set can0 type can bitrate 125000
sudo ip link set up can0
```